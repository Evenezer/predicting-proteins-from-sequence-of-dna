import numpy as np
import matplotlib.pyplot as plt

def read_fasta(filename):
    with open(filename) as f:
        hd = f.readline().strip()[1:]
        seq =''.join(x.strip() for x in f)
        return hd,seq

def make_dotplot_matrix(seqA,seqB):
    dp_matrix = np.zeros((len(seqB),len(seqA)))
    for i,c in enumerate(seqB):
        for j,d in enumerate(seqA):
            if c==d:
                dp_matrix[i,j] = 1
            #else:
            #    dp_matrix[i,j] = 0
    return dp_matrix

def dp(w,s):
    print(w,s)
    

def dp2Ascii(seq,output):
    print(seq)

def dp2Graph(m,lbl1,lbl2,hd,fn):
    plt.imshow(m)
    plt.xlabel(lbl2)
    plt.ylabel(lbl1)
    plt.title(hd)
    plt.savefig(fn)

def dp2Plt(m,lbl1,lbl2,hd,fn):
    plt.figure(figsize=(10,10))
    points = np.argwhere(m==1)
    x,y = zip(*points)
    #plt.plot(x,y,'.')
    plt.plot(x,y,'.',markersize=1)
    plt.xlabel(lbl2)
    plt.ylabel(lbl1)
    plt.title(hd)
    plt.savefig(fn)

print("Module Name:",__name__)
    
if __name__ == '__main__':
    import sys
    print(sys.argv)
    w = int(sys.argv[1])
    s = int(sys.argv[2])
    label1 = sys.argv[3]
    hd1,seq1 = read_fasta(label1)
    label2 = sys.argv[4]
    hd2,seq2 = read_fasta(label2)
    title = sys.argv[5]
    output = sys.argv[6]
    dpm = dp(w,s)
    m = make_dotplot_matrix(seq1,seq2)
    if output[-4:]=='.txt':
        dp2Ascii(s1,output)
    else:
        #dp2Graph(m,hd1,hd2,title,output)
        dp2Plt(m,hd1,hd2,title,output)