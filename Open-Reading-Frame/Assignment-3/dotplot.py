import numpy as np
import matplotlib.pyplot as plt

def dotplottt(w,s,seqAa,seqBb):
    w=w//2
    matrix =np.zeros((len(seqAa),len(seqBb)), dtype=int)
    
    for i in range(w,len(seqAa)-w):
        for j in range(w,len(seqBb)-w):
            
        
            part_seqA = seqAa[i-w:i+w+1]
            part_seqB = seqBb[j-w:j+w+1]
            c=0
            for a,b in zip(part_seqA,part_seqB):
                if a==b:
                    c+=1
                    if c==s:
                        matrix[i][j]=1
                        break# for the purpose of big files

            
    return matrix
    





def read_fasta_hd(output):
    with open(output) as f:
        hd = f.readline().strip()[1:]
        seq =''.join(x.strip() for x in f)
        return hd,seq

def read_fasta(output):
    with open(output) as f:
        hd = f.readline().strip()[1:]
        seq =''.join(x.strip() for x in f)
        return seq


def dp(w,s,seqAa,seqBb):
    hd1,seqAa=read_fasta_hd(seqAa)
    hd2,seqBb=read_fasta_hd(seqBb)
    w=w//2
    matrix =np.zeros((len(seqAa),len(seqBb)), dtype=int)

    for i in range(w,len(seqAa)-w):
        for j in range(w,len(seqBb)-w):


            part_seqAa = seqAa[i-w:i+w+1]
            part_seqBb = seqBb[j-w:j+w+1]
            c=0
            for a,b in zip(part_seqAa,part_seqBb):
                if a==b:
                    c+=1
                    if c==s:
                        matrix[i][j]=1
                        break#for the purpose of big files
    return matrix

            
    

def print_asci(seqAa,seqBb,heading,output):
    seqAa=read_fasta(seqAa)
    seqBb=read_fasta(seqBb)
    
    with open(output,'w')as f:
        print('headings',file=f)
        print('   '+seqAa,file=f)
        print('-+'+'-'*len(seqAa),file=f)
        for i,s in enumerate(seqBb):
            print(s+'|',end='',file=f)
            for j,t in enumerate(seqAa):
                if xy[i,j]==1.0:
                    print('*',end='',file=f)
                else:
                    print(' ',end='',file=f)

            print(file=f)

def dotplot2Graphics(dpm,seqAa,seqBb,output):
    hd1,seqA=read_fasta_hd(seqAa)
    hd2,seqB=read_fasta_hd(seqBb)
    plt.figure(figsize=(10,10))
    plt.title(heading)
    plt.xlabel(hd1)
    plt.ylabel(hd2)
    plt.xticks(ticks=range(len(seqA)), labels=seqA)
    plt.yticks(ticks=range(len(seqB)), labels=seqB)
    
    plt.imshow(dpm,cmap='afmhot')
    plt.savefig(output)
    

print("Module Name:",__name__)
    
if __name__ == '__main__':
    import sys
    print(sys.argv)
    w = int(sys.argv[1])
    s = int(sys.argv[2])
    seqAa = sys.argv[3]
    seqBb = sys.argv[4]
    heading=sys.argv[5]
    output = sys.argv[6]
    dpm =dp(w,s,seqAa,seqBb)
    xy=dotplottt(w,s,seqAa,seqBb)
    if output[-4:]=='.txt':
        print_asci(seqAa,seqBb,heading,output)
    else:
        dotplot2Graphics(dpm,seqAa,seqBb,output)