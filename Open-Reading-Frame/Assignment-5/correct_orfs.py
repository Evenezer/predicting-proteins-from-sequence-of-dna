import re
# writing function that get position from headers
#result should be stored in dictionary key are the ending where as the values is the beginning
def get_sequence_positionn(orf_file,gene_file):
    orf_position={}
    c_orf_p={}
    gene_position={}
    c_gene_p={}
    with open(orf_file,'r') as f, open(gene_file,'r') as g:
        orf_line=f.readlines()
        gene_line=g.readlines()
        
        
        for line in orf_line:
            if line.startswith('>'):
                
                header=line
                pattern=r':(\d+)-(\d+)'
                
                match =re.search(pattern,header)
               
                if match is not None:
                    #print(match.group(2))
                    start=int(match.group(1))
                    stop=int(match.group(2))
                    orf_position[stop]=start
        for c_line in orf_line:
            if c_line.startswith('>'):
                c_header=c_line   
                c_pattern=r':(\D*\d+)-(\d+)'
                c_match=re.search(c_pattern,c_header)
                if c_match is not None:
                    c_start=int(c_match.group(1)[1:])
                    c_stop=int(c_match.group(2))

                    c_orf_p[c_stop]=c_start
                    orf_position.update(c_orf_p)
    
        for line in gene_line:
            if line.startswith('>'):
                    
                header=line
                pattern=r':(\d+)-(\d+)'
               
                match =re.search(pattern,header)
                
                if match is not None:
                    #print(match.group(2))
                    start=int(match.group(1))
                    stop=int(match.group(2))
                    gene_position[stop]=start
        for c_line in gene_line:
            if c_line.startswith('>'):
                header=c_line
                c_pattern=r':(\D*\d+)-(\d+)'
                c_match=re.search(c_pattern,header)
                if c_match is not None:
                    c_start=int(c_match.group(1)[1:])
                    c_stop=int(c_match.group(2))
                    c_gene_p[c_stop]=c_start
                    gene_position.update(c_gene_p)
                    
        
                
    return orf_position,gene_position

def correct_orfs_(orf,gene):
    """returning total number of open reading frames, total number of genes, total number and total number and ratio reading frames correctly predicting a gene"""
    #orf,gene=get_sequence_positionn(fasta_file,file_gene)
    
    common=orf.items() & gene.items()
    common_v=orf.values() and gene.values()
    common_k=gene.keys() & orf.keys()
    return f"File: {fasta_file}'\n'File: {file_gene},'\n',Total number of open reading frames: {len(orf.items())}'\n' Total number of genes: {len(gene.items())},'\n',Total number of ratio of open reading frames: {len(common_k)/len(orf)},'\n', Total number of ratio of open reading frames pred: {len(common)/len(orf.items())},'\n', gene not found: {len(common_v)-len(common_k)} "
    
#     print('Total number of open reading frames: ',len(orf.items()))
#     print('Total number of genes: ',len(gene.items()))
#     print('Total number of ratio of open reading frames: ',len(common_k)/len(orf))
#     print('Total number of ratio of open reading frames pred.. : ', len(common)/len(orf.items()))
#     print('gene not found: ', len(common_v)-len(common_k))
#     print('length of common: ', len(common))
#     print('length of orf dict:',len(orf))
#     print('length of gene dict:', len(gene))
#     print('length of common_v: ', len(common_v))
#     print('lenght of common_k: ', len(common_k))

# taking length into account
def orf_parameter(orf,gene,parameter):
    #orf,gene=get_sequence_positionn(file_orf,file_gene)
    p=parameter*3
    for k,v in orf.items():
        for x,y in gene.items():
            if abs(k-v)>p and abs(x-y)>p:
                common=orf.items() & gene.items()
                common_v=orf.values() and gene.values()
                common_k=gene.keys() & orf.keys()
    return f"File: {fasta_file}'\n'File: {file_gene},'\n',Parameter: {p} Nucleotides,'\n',Total number of open reading frames: {len(orf.items())}'\n' Total number of genes: {len(gene.items())},'\n',Total number of ratio of open reading frames: {len(common_k)/len(orf)},'\n', Total number of ratio of open reading frames pred: {len(common)/len(orf.items())},'\n', gene not found: {len(common_v)-len(common_k)} "
    
print("Module Name:",__name__)
    
if __name__ == '__main__':
    import sys
    print(sys.argv)
    orf_file = sys.argv[1]
    file_gene =sys.argv[2]
    parameter =int(sys.argv[3])
    
    orf,gene=get_sequence_positionn(orf_file,file_gene)
    correct=correct_orfs_(orf,gene)
    print(correct)
    parameter_account=orf_parameter(orf,gene,parameter)
    print(parameter_account)
    
    
     