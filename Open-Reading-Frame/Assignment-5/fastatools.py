def write_fasta(outfile,header,sequence):
    import textwrap
    with open(outfile,'w')as ot:
        ot.write('>{}\n'.format(header))
        ot.write("\n".join(textwrap.wrap(sequence, 70)))
    print('good to go')
    
    
def single_fasta_sequence(filename):
    with open(filename) as f:
        hd=next(f)[1:-1]
        seq=''.join(line.strip() for line in f)
        
        return hd,seq