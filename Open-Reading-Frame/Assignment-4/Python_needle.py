""" Implementing the Needleman Wunsch algorithm, takes 4 parameters: gap penality, blsosum.txt, sequence1.fasta, sequence2.fasta """
# the program's output the score of the alignment and the aligned sequences, i.e the sequences plus the gaps 
import numpy as np


def read_file(filename):# it reads the blosum and return score matrices
    fh=open(filename)
    line=fh.readline()
    aa_cols=line.split()
    #print(aa_cols)
    sm={}
    for line in fh:
        entries=line.split()
        if not entries:
            continue
        #print(entries)
        row_aa=entries[0]# first entry is the aa
        scores=entries[1:]#following entries are the scores
        # match the scores with the correct column aa
        for sc,col_aa in zip(scores,aa_cols):
            #print('the pair alignment {}'. format(row_aa+col_aa,int(sc))
            sm[row_aa+col_aa]=int(sc)
        #print()
    return sm




def read_fasta(file):# this read the fasta files and return the header and sequence
    with open(file)as f:
        hd=next(f)[1:-1]
        seq=''.join(line.strip() for line in f)
    return hd,seq
    


def needle(gap,sc,s1,s2):
    DIAG=0
    LEFT=1
    UP=2
    STOP=77
    
    m=np.zeros((len(s1)+1,len(s2)+1))
    tb=np.zeros((len(s1)+1,len(s2)+1))
               
    for i in range(len(s1)+1):
        m[i,0]=i*gap
        tb[i,0]=UP       
    for j in range (len(s2)+1):
        m[0,j]=j*gap
        tb[0,j]=LEFT
    tb[0,0]=STOP            
    
    
    for i in range (1,len(s1)+1):
        for j in range(1,len(s2)+1):
            sym1=s1[i-1]
            sym2=s2[j-1]
            diag=m[i-1,j-1]+sc[sym1+sym2]
            left=m[i,j-1]+gap
            up=m[i-1,j]+gap
            m[i,j]=max(diag,left,up)
            if m[i,j]==diag:
                tb[i,j]=DIAG
            elif m[i,j]==left:
                tb[i,j]=LEFT
            else:
                tb[i,j]=UP
    # trace back
    i=len(s1)
    j=len(s2)
    a1=[]
    a2=[]
    am=[]            
    while i!=0 and j!=0:
        if tb[i,j]==DIAG:
            a1.append(s1[i-1])  
            a2.append(s2[j-1])
            if s1[i-1]==s1[j-1]:
                am.append('|') 
            elif sc[s1[i-1]+s2[j-1]]>0:
                am.append(':')
            else:
                am.append(' ')
            i-=1
            j-=1
        elif tb[i,j]==LEFT:
            a1.append('-')
            a2.append(s2[j-1])
            am.append(' ')
            j=j-1
        elif tb[i,j]==UP:
            a1.append(s1[j-1])
            a2.append('-')
            am.append(' ')
            i-=1
    a1=''.join(a1[::-1])
    a2=''.join(a2[::-1])            
    am=''.join(am[::-1])            
    return m[len(s1),len(s2) ],a1,am,a2
                
                
                
                
                
                
print("Module Name:",__name__)
    
if __name__ == '__main__':
    import sys
    print(sys.argv)
    gap = sys.argv[1]
    filename =sys.argv[2]
    seq1 = sys.argv[3]
    seq2 = sys.argv[4]
    
    h1,s1=read_fasta(seq1)
    h2,s2=read_fasta(seq2)
    sc=read_file(filename)
    s,a1,am,a2=needle(gap,sc,s1,s2 )
    #print(s)
    print(a1)            
    print(am)
    print(a2)            
                