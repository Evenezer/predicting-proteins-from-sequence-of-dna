from typing import Dict,List,Set, Optional, Tuple
from rdkit import Chem
import rdkit.Chem
#from rdkit.Chem import rdchem
from rdkit.Chem import GetPeriodicTable
from math import sqrt
import pandas as pd
import rdkit.Chem
import random
import re
from itertools import combinations,product
from itertools import combinations
from rdkit.Chem import GetPeriodicTable
from collections import defaultdict


from heapq import heapify,heappush,heappop


#from smile_generator import *

#######################################
# Molecule class
# Index free approach
# Molecules are always created as empty objects and populated using
# add_atom/add_bond methods
#######################################

class AtomNotFoundError(Exception):
    pass

class Molecule(object):

    def __init__(self):
        # A molecule consists of a set of atoms ...
        self._atom_set: Set[Atom] = set()
        # ... and a set of bonds
        self._bond_set: Set[Bond] = set()
        # For each bond keep a set of associated bonds
        self._atom_bonds: Dict[Atom,Set[Bond]] = defaultdict(set)
        # This can be used to store properties of a molecule like its name or some other info.
        self.properties: Dict[str,str] = dict()
        # These fields contain all the ring atoms and bonds. They will be calculate on demand
        # They should be updated by calling update_properties() when the molecule changes
        # This could be done "automatically" but this would complicate the code
        self._ring_atoms: Set[Atom] = None
        self._ring_bonds: Set[Bond] = None
        #self.determine_rings=None

        ### How RDKit does it
        #       RDKit is written in C++. The topology is stored as an adjacency list.
        #       Atoms are stored in a vector (Python list-like), and the index to the vector identifies an atom.
        #       For each atom a vector (Python list-like) of Bonds exist.
        #       All bonds are stored in a vector identified by their index.
        #       Here we do not use indices at all, instead atoms are identified by atom objects
        #       and bonds by bond objects
        ###
    def to_simple_smiles(self):
        ranking = cangen_ranking(self)#------------------------------<<<<<
        sm = SmilesMaker().getSimpleSmiles(self,ranking)
        return sm
    #####
    def to_randomized_smiles(self):#-----------------------------------<<<<
        dic={}
        for a in self.get_atoms():
            dic[a]=random.random()
        sm = SmilesMaker().getSimpleSmiles(self,dic)
        return sm
        
    
    
    ### Accessor methods ###
    def get_num_atoms(self) -> int:
        return len(self._atom_set)

    def get_atoms(self) -> List["Atom"]:
        return list(self._atom_set)

    def get_num_bonds(self) -> int:
        return len(self._bond_set)

    def get_bonds(self) -> List["Bond"]:
        return list(self._bond_set)

    def has_property(self, key: str) -> bool:
        return key in self.properties

    def get_property(self, key: str) -> str:
        return self.properties[key]

    def get_property_keys(self) -> List[str]:
        return list(self.properties.keys())

    def to_rdkit_with_map(self) -> Tuple[rdkit.Chem.RWMol, Dict["Atom", int]]:
        """
        Convert this molecule to an RDKit molecule
        :return (mol,map): mol tis the RDKit molecule and map is a map mapping Molecule Atom objects to RDKit atom indices
        """
        rwmol = rdkit.Chem.RWMol()  # Empty "editable" molecules
        # Dict for mapping "Atom"s to RDKit atom indices, needed for adding bonds
        atom_dict: Dict["Atom", int] = {}
        for a in self.get_atoms():
            # For each atom a:
            # Create new RDKit atom
            idx = rwmol.AddAtom(rdkit.Chem.Atom(a.get_atomic_number()))
            # ... add to RDKit molecule
            atom: rdkit.Chem.Atom = rwmol.GetAtomWithIdx(idx)
            # ... add correct number of hydrogens & formal charge
            atom.SetNoImplicit(True)
            atom.SetNumExplicitHs(a.get_hydrogen_count())
            atom.SetFormalCharge(a.get_formal_charge())
            atom_dict[a] = idx
        for b in self.get_bonds():
            # look up corresponding RDKit bond type
            bt = rdkit.Chem.BondType.values[
                int(b.get_order())] if b.get_order() != 1.5 else rdkit.Chem.BondType.AROMATIC
            # Add bond by looking up the RDKit atom indices for the bond
            rwmol.AddBond(atom_dict[b.get_first()], atom_dict[b.get_second()], bt)
        # Update properties of the molecule
        # E.g., If we had not used
        #     atom.SetNoImplicit(True)
        #     atom.SetNumExplicitHs(a.get_hydrogen_count())
        # this function would automatically add implicit hydrogens to the RDKit atoms to match the valences.
        rwmol.UpdatePropertyCache()
        return rwmol, atom_dict

    def to_rdkit(self) -> rdkit.Chem.RWMol:
        """
        Convert this molecule to an RDKit molecule
        :return:
        """
        rwmol, _ = self.to_rdkit_with_map()
        return rwmol

    def to_smiles(self) -> str:
        """
        For output we are going to "cheat". We first convert our Molecule object to an RDKit molecule and use its
        Smiles generation function
        :return: Smiles
        """
        rdmol = self.to_rdkit()
        return rdkit.Chem.MolToSmiles(rdmol)

    ### Modification methods ###

    def add_atom(self, atomic_num: int, hydrogen_count: int=0, formal_charge: int=0) -> "Atom":
        atom = Atom(self, atomic_num, hydrogen_count, formal_charge)
        self._atom_set.add(atom)
        return atom

    def remove_atom(self, atom: "Atom") -> bool:
        """
        Remove an atom and all associated bonds
        :param atom:
        :return: True if atom could be removed, False if atom does not exist
        """
        if atom in self._atom_set:
            for bond in list(self._atom_bonds[atom]): # Use copy as _atom_bonds[atom] will be modified
                self.remove_bond(bond)
            del self._atom_bonds[atom]
            self._atom_set.remove(atom)
            return True
        else:
            return False

    def add_bond(self, first, second, order) -> Optional["Bond"]:
        if second in first.get_neighbors():
            # return None if the bond already exists
            return None
        bond = Bond(self, first, second, order)
        # Add bond to set of bonds
        self._bond_set.add(bond)
        # ... and add to set of bonds for each of its atoms
        self._atom_bonds[bond.get_first()].add(bond)
        self._atom_bonds[bond.get_second()].add(bond)
        return bond

    def remove_bond(self, bond) -> bool:
        if bond in self._bond_set:
            # Remove from bond set
            self._bond_set.remove(bond)
            # and remove from atom bond sets
            self._atom_bonds[bond.get_first()].remove(bond)
            self._atom_bonds[bond.get_second()].remove(bond)
            return True
        else:
            return False

    def is_in_ring(self, atom_or_bond):
        if isinstance(atom_or_bond, Atom):
            return atom_or_bond in self._ring_atoms
        else:
            return atom_or_bond in self._ring_bonds

    def update_properties(self):#------------------------------------------------------------------- to uncomment later
        # If a Molecule changes one should call update_properties to update
        # properties dependent on the structure. These might include
        # whether atoms/bonds are in rings or aromaticity perception
        # For now the method will only update ring membership
        self.determine_rings()#--------------------------------------------------------- also this one

    def get_molecular_weight(self):
        # TODO: See Handout 2, exercise 4--------------------------------------------------task - 04
        pt=Chem.GetPeriodicTable()
        m=list(self._atom_set)
        mH=sum([x.get_hydrogen_count() for x in m])*pt.GetAtomicWeight(1)
        atoms=self.get_atoms()#-----------------------------------------------------------m.get_atoms is undefined/ use-self.get_atoms
        return (sum([pt.GetAtomicWeight(at.get_atomic_number()) for at in atoms])+mH)

    def get_connectivity_index_0(self):
        # TODO: See Handout 2, exercise 5---------------------------------------------------------- task - 5
        total = 0
        for a in self.get_atoms():
            d = a.get_degree()
            total += 1/sqrt(d)
        return total

    def get_connectivity_index_1(self):
        # TODO: See Handout 2, exercise 5
        total = 0
        for b in self.get_bonds():
            d1=b.get_first().get_degree()
            d2=b.get_second().get_degree()
            total += 1/(sqrt(d1)*sqrt(d2))
        return total

    def get_connectivity_index_2(self):
        # TODO: See Handout 2, exercise 5
        total=0
        for middle_atom in self.get_atoms():
            # from combinations we get the possiblity of having two...
            for begin_atom,end_atom in combinations(middle_atom.get_neighbors(),2):
                d1,d2,d3=(begin_atom.get_degree(),middle_atom.get_degree(),end_atom.get_degree())
                total += 1/(sqrt(d1)*sqrt(d2)*sqrt(d3))
        return total

    def _get_number_of_paths_of_length2(self):
        # TODO: See Handout 2, exercise 6  --------------------------------------------------------------- task - 6
        total2=0
        for a in self.get_atoms():
            for b in a.get_neighbors():
                for c in b.get_neighbors():
                    if a!=c: # a!=c
                        total2 += 1
        return total2//2

    def _get_number_of_paths_of_length3(self):
        # TODO: See Handout 2, exercise 6
        total2=0
        for middle_bond in self.get_bonds():
            left_middle_atom = middle_bond.get_first()
            right_middle_atom = middle_bond.get_second()
            left_neighbors = [a for a in left_middle_atom.get_neighbors() 
                              if a!=right_middle_atom]
            right_neighbors = [a for a in right_middle_atom.get_neighbors() 
                              if a!=left_middle_atom]
            for left_atom,right_atom in product(left_neighbors,right_neighbors):
                total2 += 1
        return total2

    def get_kier_1(self):
        # TODO: See Handout 2, exercise 6
        n=len(list(self._atom_set))#------------------------------- modified
        #p1=len(get_bonds())
       
        p1=len(self._bond_set)
        #print(n,p1)
        
        kier_1=float(n*((n-1)**2))/((p1)**2)
        return kier_1

    def get_kier_2(self):
        # TODO: See Handout 2, exercise 6
        n=self.get_num_atoms()
        p2=self._get_number_of_paths_of_length2()
        kier_2= ((n-1)*(n-2)**2)/(p2)**2
        return kier_2

    def get_kier_3(self):
        # TODO: See Handout 2, exercise 6
        n=self.get_num_atoms()
        p3=self._get_number_of_paths_of_length3()
        kier3=((n-3)*(n-2)**2)/(p3)**2
        kier_3=((n-1)*(n-3)**2)/(p3)**2
        if n % 2 == 0:
            return kier3
        else:
            return kier_3
    
    def determine_rings(self):
        self._ring_atoms, self._ring_bonds = Molecule._determine_rings(self.get_atoms()[0])
        
    @staticmethod
    def _determine_rings(root,path=None,visited=None,ring_atoms=None,ring_bonds=None):
        # TODO: See Handout 2, exercise 7    -----------------------------------------------------------------------task-7
        #       Implement this method by deteriming all ring atoms and bonds storing them as sets in
        #       self._ring_atoms and self._ring_bonds
        #(root,path=None,visited=None,ring_atoms=None,ring_bonds=None):
        if path is None:
            path = []
        if visited is None:
            visited = set()
        if ring_atoms is None:
            ring_atoms = set()
        if ring_bonds is None:
            ring_bonds = set()

        if root in visited:
            return

        if root in path:
            #print(root,path)
            cycle=path[path.index(root):]
            for atom in cycle:
                ring_atoms.add(atom)
            cycle_successor = cycle[1:]+cycle[:1]
            for a,b in zip(cycle,cycle_successor):
                ring_bonds.add(a.get_bond(b))
        else:
            parent = path[-1] if path else None
            path.append(root)
            #print(root)
            for nbor in root.get_neighbors():
                if nbor != parent:
                    Molecule._determine_rings(nbor,path,visited,ring_atoms,ring_bonds)
            path.pop()
        visited.add(root)
        #self.ring_atoms,self.ring_bonds=ring_atoms,ring_bonds
        return ring_atoms,ring_bonds#-----------not needed for now

    @staticmethod
    def empty() -> "Molecule":
        return Molecule()

    @staticmethod
    def from_rdkit(rdmol) -> "Molecule":
        mol = Molecule.empty()
        atoms = {} # Map RDKit atom index to Molecule Atom object
        for a in rdmol.GetAtoms():
            atom = mol.add_atom(a.GetAtomicNum(), a.GetTotalNumHs(), a.GetFormalCharge())
            atoms[a.GetIdx()] = atom
        for b in rdmol.GetBonds():
            mol.add_bond(atoms[b.GetBeginAtomIdx()], atoms[b.GetEndAtomIdx()], b.GetBondTypeAsDouble())
        mol.update_properties()#--------------------------------------------------------------------------------uncomment it later
        return mol

    @staticmethod
    def from_smiles(smiles) -> "Molecule":
        rdmol = rdkit.Chem.MolFromSmiles(smiles)
        return Molecule.from_rdkit(rdmol)
    
    @staticmethod
    def classify_BM_atom(mol):
        """
        Classifies atoms in a molecule as side chain, ring, or linker atoms.

        """
        # Initialize empty classification dictionary
        classification = {}

        # Iterate over atoms in the molecule
        for atom in mol.get_atoms():
            # Get atom index
            atom_index = atom

            # Get neighbors of atom
            neighbors = [bond.get_other(atom) for bond in atom.get_bonds()]#?

            # Check if atom is in a ring
            in_ring = atom.is_in_ring()

            # If atom is in a ring, classify as ring atom
            if in_ring:
                classification[atom_index] = 'ring'

            # If atom is not in a ring, check if it has only one neighbor
            elif len(neighbors) == 1:
                # If atom has only one neighbor, classify as linker atom
                classification[atom_index] = 'side chain'

            # If atom has more than one neighbor, classify as side chain atom
            else:
                classification[atom_index] = 'linkers'
        while True:
            change=False
            for atom in mol.get_atoms():
                if classification[atom]=="linkers":
                    sc_neighbors = [nbor for nbor in atom.get_neighbors() if classification[nbor]=="side chain"]
                    if atom.get_degree()-len(sc_neighbors)==1:
                        classification[atom]="side chain"
                        change = True
            if not change:
                break
        mol._classification = classification
        return classification

    def _ipython_display_(self):# added by pro.martin
        from IPython.display import display
        return display(self.to_rdkit())
    

class Atom(object):

    def __init__(self, mol: Molecule, atomic_number: int, hydrogen_count: int, formal_charge: int):
        self._mol = mol
        self._atomic_number = atomic_number
        self._hydrogen_count = hydrogen_count
        self._formal_charge = formal_charge

    def get_atomic_number(self) -> int:
        return self._atomic_number

    def get_atomic_symbol(self) -> str:
        return GetPeriodicTable().GetElementSymbol(self._atomic_number)

    def get_hydrogen_count(self) -> int:
        """
        :return: number of (implicitly) attached hydrogens
        """
        return self._hydrogen_count

    def is_aromatic(self) -> bool:
        return any(map(lambda x: x.is_aromatic(), self.get_bonds()))

    def is_in_ring(self) -> bool:
        return self._mol.is_in_ring(self)

    def get_formal_charge(self) -> int:
        return self._formal_charge

    def get_bond(self, other: "Atom") -> "Bond":
        """
        :param other:
        :return: Bond between self and other if it exists else None
        """
        assert other != self
        for bond in self.get_bonds():
            if bond.get_first() == other or bond.get_second() == other:
                return bond
        return None

    def get_bonds(self) -> List["Bond"]:
        """
        :return: Bonds of this atom
        """
        return list(self._mol._atom_bonds[self])

    def get_neighbors(self) -> List["Atom"]:
        """
        :return: neighboring atoms
        """
        return [bond.get_other(self) for bond in self._mol._atom_bonds[self]]

    def get_degree(self) -> int:
        """
        :return: the number of neighboring atoms (excluding implicit hydrogens)
        """
        return len(self._mol._atom_bonds[self])

    def set_atomic_number(self, atomic_number: int):
        self._atomic_number = atomic_number

    def set_hydrogen_count(self, hydrogen_count: int):
        self._hydrogen_count = hydrogen_count

    def set_formal_charge(self, formal_charge: int):
        self._formal_charge = formal_charge

    def __str__(self):
        if self.get_hydrogen_count()>0:
            return f"{self.get_atomic_symbol()}H{self.get_hydrogen_count()}"
        else:
            return self.get_atomic_symbol()
    def __repr__(self):
        return f"<{self}>"

class Bond(object):
    def __init__(self, mol, first_atom: Atom, second_atom: Atom, order: float):
        self._mol = mol
        self._first = first_atom
        self._second = second_atom
        if id(self._second) < id(self._first):
            # This ensures that atoms are stored in a well defined order.
            self._first, self._second = self._second, self._first
        self._order = order

    def get_order(self) -> float:
        return self._order

    def is_aromatic(self) -> bool:
        return self.get_order() == 1.5

    def is_in_ring(self) -> bool:
        return self._mol.is_in_ring(self)

    def get_first(self) -> Atom:
        return self._first

    def get_second(self) -> Atom:
        return self._second

    def get_other(self, atom) -> Atom:
        """
        :param atom: one atom of the bond
        :return: the other atom of the bond
        """
        if self._first == atom:
            return self._second
        if self._second == atom:
            return self._first
        return None

    def set_order(self, order: float):
        self._order = order

    def __str__(self):
        symbols={1:'-',2:"=",3:"#",1.5:":"}
        return f"{self.get_first()}{symbols[self.get_order()]}{self.get_second()}"
    
    def __repr__(self):
        return f"<{self}>"
    
    
    
    
####################################################
class SmilesMaker:
    
    def __init__(self,rankingMethod= None):

        self.rankingMethod = rankingMethod
        
        # fields used during Smiles generation
        self.atomRanks = None
        self.visited = None
        self.ancestor = None
        self.openingClosures = None
        self.closingClosures = None
        self.digits = None
        self.ranks = None
        
    @staticmethod
    def _getBondSymbol(mol,a,b):
            "return bond symbol between atoms a and b"
            bond = a.get_bond(b)
            b = bond.get_order()
            if b==1.0: return ""
            if b==1.5: return ""
            if b==2.0: return "="
            if b==3.0: return "#"        
    
    
    def getSimpleSmiles(self, mol,ranks=defaultdict(lambda: 0)):#------------------exercise-1
        """
        return a simplified version of Smiles
        mol is assumed not to contain individual hydrogen atom objects
        multiple fragment Smiles are not supported
        optional parameter ranks defines the priorities of atom ranks
        """    
        # atom prioritization
        if not ranks:
            if self.rankingMethod:
                self.atomRanks = self.rankingMethod(mol)
            else:
                self.atomRanks = defaultdict(lambda: 0)
        else:
            self.atomRanks = ranks

        # retrieve root atom with highest priority
        atoms = sorted(mol.get_atoms(),key=lambda a: self.atomRanks[a])
        root = atoms[0]

        # keep track of visited atoms
        self.visited=set()
        # keep track of ancestor atoms
        self.ancestor=set()
        # key: atom to be opened, values: closing atoms
        self.openingClosures=defaultdict(lambda: [])
        self.getClosures(mol,root,None)

        # key: atom to be closed, values: bond symbol + closure numbers
        self.closingClosures=defaultdict(lambda: [])
        # list of digits available for closures (only single digits supported)
        # heap operqations are used on digits to always retrieve
        # the smallest digit available
        
        # Only support closure digits from 1 to 9
        self.digits = [str(x) for x in range(1,10)]
        # no need to call heapify as a sorted list fulfills the heap property

        self.visited = set() # reset visited atoms
        return self.buildSmiles(mol,root,None)



    def getClosures(self,mol,atom,parent):
        self.ancestor.add(atom) # not every visited atom is ancestor
        self.visited.add(atom)
        atomObj = atom
        nbors = [a for a in atomObj.get_neighbors() if a!=parent]
        nbors.sort(key=lambda a: self.atomRanks[a])
        for n in nbors:
            if n in self.ancestor:
                self.openingClosures[n].append(atom)
            elif n not in self.visited:
                self.getClosures(mol,n,atom)
        self.ancestor.remove(atom)
  
    def buildSmiles(self,mol,atom,parent):
        self.visited.add(atom)
        # Each call constructs a partial Smiles starting with
        # bond symbol + atom symbol
        # Only the root atom has no preceding bond symbol
        seq = ""
        if parent is not None:
            seq += self._getBondSymbol(mol,parent,atom)
        atomObj = atom
        symbol = atomObj.get_atomic_symbol()#----------------------------<>-----------------
        if atomObj.is_aromatic():
            symbol = symbol.lower()
            # Check special case aromatic N + H use '[nH]' instead of 'n'
            if symbol=='n' and atomObj.get_hydrogen_count()==1:#-----------------------------<<
                symbol = '[nH]'
        if atomObj.get_formal_charge()==1:
            symbol=f"[{symbol}+]"
        if atomObj.get_formal_charge()==-1:
            symbol=f"[{symbol}-]"
        seq += symbol 

        # add ring closure closers
        for d in self.closingClosures[atom]:
            seq += d
            # The digit is freed and can be used again
            heappush(self.digits,d[-1])
        # add ring closure openers
        for a in self.openingClosures[atom]:
            # a new digit is taken from digits
            d = self._getBondSymbol(mol,atom,a)+str(heappop(self.digits))
            seq += d
            self.closingClosures[a].append(d)

        nbors = [a for a in atomObj.get_neighbors() if a!=parent]
        nbors.sort(key=lambda a: self.atomRanks[a])
        branches = [] # Smiles for all branches
        for n in nbors:
            if n not in self.visited:
                branches.append(self.buildSmiles(mol,n,atom))
        # The first branches have to be parenthesized
        for branch in branches[:-1]:
            seq += "("+branch+")"
        # tha last (i.e. main) branch does not get parentheses
        if len(branches)>0:
            seq += branches[-1]
        return seq
    @staticmethod#------------------------------------------------------------------------------------------------------------------..
    def from_simple_smiles(smiles:str) -> Molecule:
        """Return Molecule objects directly from a Smiles string """
        mole_object=[Molecule.from_smiles(smile) for smile in smiles]
        

        #re.findall(r'###', mole_object)
        return mole_object
    @staticmethod
    def tokenized_nested(smile):#---------------------------------------------------------------------------------------<<<
        alternatives = [r"\(",r"\)",r"([-:=#])?(Br?|Cl?|N|O|S|P|F|I|b|c|n|o|s|p|\[nH\])(\d*)"]
        re_token = "("+"|".join(alternatives)+")"
        re_token_re=re.compile(re_token)
        tokenized = [s[0] for s in re.findall(re_token,smile)]
        t=str(tokenized).replace("'(',","[").replace("')'","]")
        #print((type(t)))
        return eval(t)#,type(t)
    
    
    @staticmethod
    def build_molecule(mol,liste,previous=None,prev_lower=False,closure_atoms=None):#-----------------------------------------------<<<
        if closure_atoms is None:
            closure_atoms= {}
        for atom_token in liste:
            if type(atom_token)==list:
                build_molecule(mol,atom_token,previous,prev_lower,closure_atoms)
            else:
                whole,bond_symbol,atom_symbol,ring_closures=re.match(re_token,atom_token).groups()
                hydrogen_count = 1 if atom_symbol=="[nH]" else 0
                atom= mol.add_atom(atom_numbers[atom_symbol.upper()],hydrogen_count)
                bond_order=bond_symbol_to_order[bond_symbol]
                if bond_order==0 and prev_lower and atom_symbol.islower():
                    bond_order=1.5
                else:
                    bond_order=1

                rc=ring_closures
                if rc:
                    if rc not in closure_atoms:
                        closure_atoms[rc] = (atom,atom_symbol.islower())
                    else:
                        rc_atom,rc_lower = closure_atoms[rc]
                        del closure_atoms[rc]
                        rc_bond_order = 1.5 if rc_lower and atom_symbol.islower() else 1
                        mol.add_bond(atom,rc_atom,rc_bond_order)

                if previous is not None:
                    mol.add_bond(atom,previous,bond_order)
                    #add_bond to previous
                previous = atom
                prev_lower = atom_symbol.islower()
        return mol
    
    
    @staticmethod
    def from_simple_smiles(s):#----------------------------------------------------------------------------------------<<<<
        valence= {"C":4,"O":2,"N":3,"Cl":1,"S":2,"F":1,"Br":1,"I":1}
        mol=Molecule.empty()
        atom_numbers={'C':6,'N':7,'O':8,'CL':17,'[NH]':7,'S':16,'F':9,'BR':35,'I':53}
        bond_symbol_to_order={None:0,"":0,"-":1,"=":2,"#":3,":":1.5}
        #first function
        alternatives = [r"\(",r"\)",r"([-:=#]?)(Br?|Cl?|N|O|S|P|F|I|b|c|n|o|s|p|\[nH\])(\d*)"]
        re_token = "("+"|".join(alternatives)+")"
        re_token_re=re.compile(re_token)
        def tokenized_nested(s):#---------------------------------------------------------------------------------------<<<
            #alternatives = [r"\(",r"\)",r"([-:=#])?(Br?|Cl?|N|O|S|s?|P|F|I|b|c|n|o|s|p|\[nH\])(\d*)"]
            #re_token = "("+"|".join(alternatives)+")"
            #re_token_re=re.compile(re_token)
            tokenized = [s[0] for s in re.findall(re_token,s)]
            t=str(tokenized).replace("'(',","[").replace("')'","]")
            #print((type(t)))
            return eval(t)#,type(t)
        def build_molecule(mol,liste,previous=None,prev_lower=False,closure_atoms=None):#-----------------------------------------------<<<
            if closure_atoms is None:
                closure_atoms= {}
            for atom_token in liste:
                if type(atom_token)==list:
                    build_molecule(mol,atom_token,previous,prev_lower,closure_atoms)
                else:
                    whole,bond_symbol,atom_symbol,ring_closures=re.match(re_token,atom_token).groups()
                    hydrogen_count = 1 if atom_symbol=="[nH]" else 0
                    if atom_symbol=="[nH]":
                        atom_symbol="n"
                    atom= mol.add_atom(atom_numbers[atom_symbol.upper()],hydrogen_count)#atom_symbol.upper()
                    bond_order=bond_symbol_to_order[bond_symbol]
                    if bond_order==0:
                        if prev_lower and atom_symbol.islower():
                            bond_order=1.5
                        else:
                            bond_order=1

                    rc=ring_closures
                    for rc in ring_closures:
                        if rc not in closure_atoms:
                            closure_atoms[rc] = (atom,atom_symbol.islower())
                        else:
                            rc_atom,rc_lower = closure_atoms[rc]
                            del closure_atoms[rc]
                            rc_bond_order = 1.5 if rc_lower and atom_symbol.islower() else 1
                            mol.add_bond(atom,rc_atom,rc_bond_order)

                    if previous is not None:
                        mol.add_bond(atom,previous,bond_order)
                        #add_bond to previous
                    previous = atom
                    prev_lower = atom_symbol.islower()
            return mol
        def add_hydrogens(mol):
            for atom in mol.get_atoms():
                if atom.get_hydrogen_count()==0:
                    bo_sum = int(sum(bond.get_order() for bond in atom.get_bonds()))
                    hc = valence[atom.get_atomic_symbol()]-bo_sum
                    if hc>0:
                        atom.set_hydrogen_count(hc)
            return mol
        mol = build_molecule(mol,tokenized_nested(s))
        return add_hydrogens(mol)
        return build_molecule(mol,tokenized_nested(s))
        
        
    
def invariants(mol: Molecule):
    "Determine initial atom invariants"
    inv = dict()
    for atom in mol.get_atoms():#---------------------------------------------------------------------<<<
        i1 = atom.get_degree()#bondorder#get_order#get_degree
        i2 = atom.get_hydrogen_count()#consider adding self
        #i2 = atom.GetTotalValence()-atom.GetTotalNumHs()
        #i3 = atom.GetAtomicNum()
        i3=atom.get_atomic_number()
        #i4 = 1 if atom.get_formal_charge() < 0 else 0
        i4=0# assuming no formal charges
        #i4 = 1 if atom.GetFormalCharge() < 0 else 0
        #i5 = abs(atom.GetFormalCharge())
        i5=0
        i6 = atom.get_hydrogen_count()
        inv[atom] = int("%1d%02d%02d%1d%1d%1d"%(i1,i2,i3,i4,i5,i6))
    return inv

def invToRanks(inv,oldRanks):
    """Convert invariant to ranks consistent with ranking of oldRanks
       return rmap of ranks and the number of different ranks
    """
    sortedAtoms = sorted(inv.keys(),key=lambda atom: (oldRanks[atom],inv[atom]))
    # When assigning ranks to the sorted atoms treat ties properly
    rk = 0
    prev = -1
    ranks = dict()
    for a in sortedAtoms:
        if (oldRanks[a],inv[a]) != prev:
            rk+=1
            prev = (oldRanks[a],inv[a]) 
        ranks[a] = rk
    return ranks,rk


import math
def checkPrime(v):
    "check whether v is prime"
    limit = int(math.sqrt(v))+1
    for d in range(3,limit,2):
        if v%d==0:
            return False
    return True
def getPrimeList(numPrimes):
    "Retrieve a list of numPrimes primes"
    pl = [0,2]
    n = 3
    while len(pl)<numPrimes:
        if checkPrime(n):
            pl.append(n)
        n += 2
    return pl

primes = getPrimeList(1000)
def new_ranks(mol,ranks):
    "Calculate a new set of ranks from old ranks"
    atom_primes = {}
    for a in mol.get_atoms():
        i = a#a.GetIdx()
        atom_primes[i]=primes[ranks[i]]
    inv = dict()
    for a in mol.get_atoms():
        prod = 1
        for n in a.get_neighbors():
            prod *= atom_primes[n]
        inv[a] = prod
    return invToRanks(inv,ranks)

def canon_iter(mol,ranks):
    old_rank_count = 0
    rank_count = 1
    while old_rank_count < rank_count and rank_count < len(ranks):
        old_rank_count = rank_count
        ranks,rank_count = new_ranks(mol,ranks)
    return ranks,rank_count

def break_ties(mol,ranks):
    "break a some tied ranks"
    rankCount = [0]*len(ranks)
    for r in ranks.values():
        rankCount[r] += 1

    # find smallest duplicate rank (rj)
    rk=0
    while rankCount[rk] <= 1:
        rk += 1

    inv = dict() # new invariants
    for a in ranks.keys():
        inv[a] = 2*ranks[a]

    # change invariant of one of the smallest duplicate ranks
    for a in ranks.keys():
        if ranks[a] == rk:
            inv[a] -= 1
            break
    return invToRanks(inv,ranks)

def cangen_ranking(mol):
    inv = invariants(mol)
    default = defaultdict(int)
    ranks,rank_count = invToRanks(inv,default)#invTRanks(inv,[1]*len(inv))
    while rank_count < len(ranks):
        ranks,rank_count = canon_iter(mol,ranks)
        if rank_count < len(ranks):
            ranks,rank_count = break_ties(mol,ranks)
    return ranks

