from collections import defaultdict
from rdkit import Chem
from heapq import heapify,heappush,heappop

class SmilesMaker:
    
    def __init__(self,rankingMethod= None):

        self.rankingMethod = rankingMethod
        
        # fields used during Smiles generation
        self.atomRanks = None
        self.visited = None
        self.ancestor = None
        self.openingClosures = None
        self.closingClosures = None
        self.digits = None
        self.ranks = None
        
    @staticmethod
    def _getBondSymbol(mol,a,b):
            "return bond symbol between atoms a and b"
            bond = mol.GetBondBetweenAtoms(a,b)
            b = bond.GetBondTypeAsDouble()
            if b==1.0: return ""
            if b==1.5: return ""
            if b==2.0: return "="
            if b==3.0: return "#"        
    
    
    def getSimpleSmiles(self, mol,ranks=defaultdict(lambda: 0)):#------------------exercise-1
        """
        return a simplified version of Smiles
        mol is assumed not to contain individual hydrogen atom objects
        multiple fragment Smiles are not supported
        optional parameter ranks defines the priorities of atom ranks
        """    
        # atom prioritization
        if not ranks:
            if self.rankingMethod:
                self.atomRanks = self.rankingMethod(mol)
            else:
                self.atomRanks = defaultdict(lambda: 0)
        else:
            self.atomRanks = ranks

        # retrieve root atom with highest priority
        atoms = sorted([a.GetIdx() for a in mol.GetAtoms()],key=lambda a: self.atomRanks[a])
        root = atoms[0]

        # keep track of visited atoms
        self.visited=set()
        # keep track of ancestor atoms
        self.ancestor=set()
        # key: atom to be opened, values: closing atoms
        self.openingClosures=defaultdict(lambda: [])
        self.getClosures(mol,root,None)

        # key: atom to be closed, values: bond symbol + closure numbers
        self.closingClosures=defaultdict(lambda: [])
        # list of digits available for closures (only single digits supported)
        # heap operqations are used on digits to always retrieve
        # the smallest digit available
        
        # Only support closure digits from 1 to 9
        self.digits = [str(x) for x in range(1,10)]
        # no need to call heapify as a sorted list fulfills the heap property

        self.visited = set() # reset visited atoms
        return self.buildSmiles(mol,root,None)


    def getClosures(self,mol,atom,parent):
        self.ancestor.add(atom) # not every visited atom is ancestor
        self.visited.add(atom)
        atomObj = mol.GetAtomWithIdx(atom)
        nbors = [a.GetIdx() for a in atomObj.GetNeighbors() if a.GetIdx()!=parent]
        nbors.sort(key=lambda a: self.atomRanks[a])
        for n in nbors:
            if n in self.ancestor:
                self.openingClosures[n].append(atom)
            elif n not in self.visited:
                self.getClosures(mol,n,atom)
        self.ancestor.remove(atom)
  
    def buildSmiles(self,mol,atom,parent):
        self.visited.add(atom)
        # Each call constructs a partial Smiles starting with
        # bond symbol + atom symbol
        # Only the root atom has no preceding bond symbol
        seq = ""
        if parent is not None:
            seq += self._getBondSymbol(mol,parent,atom)
        atomObj = mol.GetAtomWithIdx(atom)
        symbol = atomObj.GetSymbol()
        if atomObj.GetIsAromatic():
            symbol = symbol.lower()
            # Check special case aromatic N + H use '[nH]' instead of 'n'
            if symbol=='n' and atomObj.GetTotalNumHs()==1:
                symbol = '[nH]'
        seq += symbol 

        # add ring closure closers
        for d in self.closingClosures[atom]:
            seq += d
            # The digit is freed and can be used again
            heappush(self.digits,d[-1])
        # add ring closure openers
        for a in self.openingClosures[atom]:
            # a new digit is taken from digits
            d = self._getBondSymbol(mol,atom,a)+str(heappop(self.digits))
            seq += d
            self.closingClosures[a].append(d)

        nbors = [a.GetIdx() for a in atomObj.GetNeighbors() if a.GetIdx()!=parent]
        nbors.sort(key=lambda a: self.atomRanks[a])
        branches = [] # Smiles for all branches
        for n in nbors:
            if n not in self.visited:
                branches.append(self.buildSmiles(mol,n,atom))
        # The first branches have to be parenthesized
        for branch in branches[:-1]:
            seq += "("+branch+")"
        # tha last (i.e. main) branch does not get parentheses
        if len(branches)>0:
            seq += branches[-1]
        return seq

    
def invariants(mol):
    "Determine initial atom invariants"
    inv = dict()
    for atom in mol.get_atoms():
        i1 = atom.get_degree()#bondorder#get_order#get_degree
        i2 = atom.get_hydrogen_count()#consider adding self
        #i2 = atom.GetTotalValence()-atom.GetTotalNumHs()
        #i3 = atom.GetAtomicNum()
        i3=atom.get_atomic_number()
        #i4 = 1 if atom.get_formal_charge() < 0 else 0
        i4=0# assuming no formal charges
        #i4 = 1 if atom.GetFormalCharge() < 0 else 0
        #i5 = abs(atom.GetFormalCharge())
        i5=0
        i6 = atom.get_hydrogen_count()
        inv[atom] = int("%1d%02d%02d%1d%1d%1d"%(i1,i2,i3,i4,i5,i6))
    return inv

def invToRanks(inv,oldRanks):
    """Convert invariant to ranks consistent with ranking of oldRanks
       return rmap of ranks and the number of different ranks
    """
    sortedAtoms = sorted(inv.keys(),key=lambda atom: (oldRanks[atom],inv[atom]))
    # When assigning ranks to the sorted atoms treat ties properly
    rk = 0
    prev = -1
    ranks = dict()
    for a in sortedAtoms:
        if (oldRanks[a],inv[a]) != prev:
            rk+=1
            prev = (oldRanks[a],inv[a]) 
        ranks[a] = rk
    return ranks,rk

def new_ranks(mol,ranks):
    "Calculate a new set of ranks from old ranks"
    atom_primes = {}
    for a in mol.GetAtoms():
        i = a#a.GetIdx()
        atom_primes[i]=primes[ranks[i]]
    inv = dict()
    for a in mol.GetAtoms():
        prod = 1
        for n in a.GetNeighbors():
            prod *= atom_primes[n.GetIdx()]
        inv[a.GetIdx()] = prod
    return invToRanks(inv,ranks)

def canon_iter(mol,ranks):
    old_rank_count = 0
    rank_count = 1
    while old_rank_count < rank_count and rank_count < len(ranks):
        old_rank_count = rank_count
        ranks,rank_count = new_ranks(mol,ranks)
    return ranks,rank_count

def break_ties(mol,ranks):
    "break a some tied ranks"
    rankCount = [0]*len(ranks)
    for r in ranks.values():
        rankCount[r] += 1

    # find smallest duplicate rank (rj)
    rk=0
    while rankCount[rk] <= 1:
        rk += 1

    inv = dict() # new invariants
    for a in ranks.keys():
        inv[a] = 2*ranks[a]

    # change invariant of one of the smallest duplicate ranks
    for a in ranks.keys():
        if ranks[a] == rk:
            inv[a] -= 1
            break
    return invToRanks(inv,ranks)

def cangen_ranking(mol):
    inv = invariants(mol)
    ranks,rank_count = invToRanks(inv,len(inv))#invToRanks(inv,[1]*len(inv))
    while rank_count < len(ranks):
        ranks,rank_count = canon_iter(mol,ranks)
        if rank_count < len(ranks):
            ranks,rank_count = break_ties(mol,ranks)
    return ranks